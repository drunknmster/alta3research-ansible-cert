# alta3research-ansible-cert



## Getting started

The purpose of this playbook is to showcase skills and knowledge I learned from my training.  Not everything is in here, but I tried to add several items we covered.  

To run this playbook, I use the following command `ansible-playbook standupenv.yml --ask-vault-pass`

I created a secret file with my sudo password ( not in repo) using `ansible-vault create secret`

I added the output.txt to show successful run.

## Try it out
If you wish to run this on your own, here are the steps required.  


- You will need to have docker, docker-compose, python3, python docker sdk, and python docker-compose
- Get Docker Compose library from my github page.  `git clone https://github.com/mike-a-davis/docker-lighthouse.git`

- Change the var `project_path` to be the path of your cloned env
- Set up your secret file with command above
